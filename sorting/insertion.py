def insertion_sort(test_data: list[int]) -> list[int]:
    data = test_data[:]

    # Iterate over each element starting from 1 (0 is considered as sorted by default).
    for i in range(1, len(data)):
        # Iterate over all elements on the left side.
        for j in range(i - 1, -1, -1):
            if data[j] > data[j + 1]:
                data[j + 1], data[j] = data[j], data[j + 1]

    return data
