def bubble_sort(test_data: list[int]) -> list[int]:
    data = test_data[:]

    for _ in range(len(data)):
        for j in range(len(data) - 1):
            if data[j] > data[j + 1]:
                data[j], data[j + 1] = data[j + 1], data[j]

    return data


def bubble_sort_optimized(test_data: list[int]) -> list[int]:
    data = test_data[:]

    for i in range(len(data)):
        swapped = False

        for j in range(len(data) - 1 - i):
            if data[j] > data[j + 1]:
                data[j], data[j + 1] = data[j + 1], data[j]
                swapped = True

        if not swapped:
            break

    return data
