def merge_sort(test_data: list[int]) -> list[int]:
    if len(test_data) == 1:
        return test_data

    data = test_data[:]

    mid = len(data) // 2
    left = data[:mid]
    right = data[mid:]

    left = merge_sort(left)
    right = merge_sort(right)

    left_idx = 0
    right_idx = 0
    data_idx = 0

    # Use elements from both sub-lists if both are non-empty.
    while left_idx < len(left) and right_idx < len(right):
        if left[left_idx] < right[right_idx]:
            data[data_idx] = left[left_idx]
            left_idx += 1
        else:
            data[data_idx] = right[right_idx]
            right_idx += 1

        data_idx += 1

    # Use elements from remaining sub-lists (by now only one should be non-empty).
    if left_idx < len(left):
        data[data_idx:] = left[left_idx:]

    if right_idx < len(right):
        data[data_idx:] = right[right_idx:]

    return data
